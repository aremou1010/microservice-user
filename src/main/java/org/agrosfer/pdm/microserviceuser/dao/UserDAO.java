/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.dao;

import java.util.Optional;
import org.agrosfer.pdm.microserviceuser.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface UserDAO extends JpaRepository<User, Long> {
    
    Optional<User> findByLogin(String login);
    
    Optional<User> findByLoginAndIsActivatedTrue(String login);
    
    Optional<User> findByReferenceAndIsDeleteFalse(String ref);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.dao;

import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microserviceuser.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface RoleDAO extends JpaRepository<Role, Long> {
    
    List<Role> findAllByIsDeleteFalse();
    
    Optional<Role> findByDesignationAndIsDeleteFalse(String designation);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.configs;

import org.agrosfer.pdm.microserviceuser.helpers.UserHelper;
import org.agrosfer.pdm.microserviceuser.model.User;

/**
 *
 * @author g3a
 */
public interface UserMapper {
    
    public static UserHelper fromUser(User newUser){
        
        UserHelper aUser = new UserHelper();
        
        aUser.setId(newUser.getId());
        aUser.setReference(newUser.getReference());
        aUser.setLogin(newUser.getLogin());
        aUser.setPassword(newUser.getPassword());
        aUser.setSalt(newUser.getSalt());
        aUser.setIsActivated(newUser.getIsActivated());
        aUser.setIsAdmin(newUser.getIsAdmin());
        aUser.setStatus(newUser.getStatus());
        aUser.setCreateAt(newUser.getCreateAt());
        aUser.setUpdateAt(newUser.getUpdateAt());
        aUser.setDeleteAt(newUser.getDeleteAt());
        aUser.setIsDelete(newUser.getIsDelete());
        aUser.setRoles(newUser.getRoles());
        aUser.setClientReference(newUser.getClientReference());

        return aUser;
    }
    
}

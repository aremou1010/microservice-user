/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.configs;

import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author g3a
 */
@Component
@ConfigurationProperties(prefix = "ms-user-configs")
@Data
public class ApplicationPropertiesConfig {
    
    String url;
    String msCodificationCreateUrl;
    String msClientCreateUrl;
    String msUtilsEncryptPasswordUrl;
    String msUtilsDecryptPasswordUrl;
    
    Map<String, String> resources;
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.helpers;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
public class UserRegisterHelper {
    
    //User
    @Column(unique = true)
    private String login;
    
    private String password;
    
    //Client
    private String firstName;
    
    private String lastName;
    
    @Column(unique = true)
    private String phone;
    
    private String address;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    
    private String email;
    
    private String website;
    
    private Boolean isCompany;
    
    @Column(unique = true)
    private String companyName;
    
    @ElementCollection
    private List<String> fieldsOfActivity;
    
    @ElementCollection
    private List<String> functions;

    public UserRegisterHelper() {
    }

    public UserRegisterHelper(String login, String password, String firstName, String lastName, String phone, String address, Date birthDate, String email, String website, Boolean isCompany, String companyName, List<String> fieldsOfActivity, List<String> functions) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.birthDate = birthDate;
        this.email = email;
        this.website = website;
        this.isCompany = isCompany;
        this.companyName = companyName;
        this.fieldsOfActivity = fieldsOfActivity;
        this.functions = functions;
    }
    
}

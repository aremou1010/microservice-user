/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.helpers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;
import org.agrosfer.pdm.microserviceuser.model.UserStatus;

/**
 *
 * @author g3a
 */
@Data
public class UserHelper {
    
    private Long id;
    
    private String reference;
    
    private String login;
    
    @JsonIgnore
    private String password;
    
    @JsonIgnore
    private String salt;
    
    private Boolean isActivated;
    
    private Boolean isAdmin;
    
    private UserStatus status;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;
    
    private List<String> roles;
    
    private String clientReference;

    public UserHelper() {
    }

    public UserHelper(Long id, String reference, String login, String password, String salt, Boolean isActivated, Boolean isAdmin, UserStatus status, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete, List<String> roles, String clientReference) {
        this.id = id;
        this.reference = reference;
        this.login = login;
        this.password = password;
        this.salt = salt;
        this.isActivated = isActivated;
        this.isAdmin = isAdmin;
        this.status = status;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
        this.roles = roles;
        this.clientReference = clientReference;
    }
    
}

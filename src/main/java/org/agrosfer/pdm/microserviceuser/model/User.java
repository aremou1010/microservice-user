/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
@Table(name = "users")
public class User implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String reference;
    
    @Column(unique = true)
    private String login;
    
    private String password;
    
    private Boolean isAdmin;
    
    @JsonIgnore
    private String salt;
    
    private Boolean isActivated;
    
    private UserStatus status;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;
    
    @ElementCollection
    private List<String> roles;
    
    //@Column(unique = true)
    private String clientReference;

    public User() {
    }

    public User(Long id, String reference, String login, String password, Boolean isAdmin, String salt, Boolean isActivated, UserStatus status, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete, List<String> roles, String clientReference) {
        this.id = id;
        this.reference = reference;
        this.login = login;
        this.password = password;
        this.isAdmin = isAdmin;
        this.salt = salt;
        this.isActivated = isActivated;
        this.status = status;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
        this.roles = roles;
        this.clientReference = clientReference;
    }
    
}

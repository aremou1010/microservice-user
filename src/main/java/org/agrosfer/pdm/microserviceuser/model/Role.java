/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
public class Role implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(unique = true)
    private String designation;
    
    @ElementCollection
    private Map<String, String> resources;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;

    public Role() {
    }

    public Role(Long id, String designation, Map<String, String> resources, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete) {
        this.id = id;
        this.designation = designation;
        this.resources = resources;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
    }
    
}

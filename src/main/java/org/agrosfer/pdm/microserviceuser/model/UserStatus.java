/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.model;

/**
 *
 * @author g3a
 */
public enum UserStatus {
    
    BEING_CREATED, ACTIVATED, SUSPEND, DEACTIVATED;
    
    public boolean valid(UserStatus currentStatus, UserStatus newStatus) {

    if (currentStatus == BEING_CREATED) {
      return newStatus == ACTIVATED;
    } else if (currentStatus == ACTIVATED) {
      return newStatus == SUSPEND || newStatus == DEACTIVATED;
    } else if (currentStatus == SUSPEND) {
      return newStatus == ACTIVATED || newStatus == DEACTIVATED;
    } else if (currentStatus == DEACTIVATED) {
      return false;
    } else {
      throw new RuntimeException("Unrecognized situation.");
    }

//    if (null == currentStatus) {
//        throw new RuntimeException("Unrecognized situation.");
//    } else
//        switch (currentStatus) {
//            case BEING_CREATED:
//                return newStatus == ACTIVATED;
//            case ACTIVATED:
//                return newStatus == SUSPEND || newStatus == DEACTIVATED;
//            case SUSPEND:
//                return newStatus == ACTIVATED || newStatus == DEACTIVATED;
//            case DEACTIVATED:
//                return false;
//            default:
//                throw new RuntimeException("Unrecognized situation.");
//        }
    
    
  }
    
}

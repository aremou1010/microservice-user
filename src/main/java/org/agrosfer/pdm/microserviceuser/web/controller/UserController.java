/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceuser.web.controller;

import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microserviceuser.beans.ClientBean;
import org.agrosfer.pdm.microserviceuser.beans.ReferenceBean;
import org.agrosfer.pdm.microserviceuser.configs.ApplicationPropertiesConfig;
import org.agrosfer.pdm.microserviceuser.configs.UserMapper;
import org.agrosfer.pdm.microserviceuser.dao.RoleDAO;
import org.agrosfer.pdm.microserviceuser.dao.UserDAO;
import org.agrosfer.pdm.microserviceuser.helpers.PassGen;
import org.agrosfer.pdm.microserviceuser.helpers.UserHelper;
import org.agrosfer.pdm.microserviceuser.helpers.UserRegisterHelper;
import org.agrosfer.pdm.microserviceuser.model.Role;
import org.agrosfer.pdm.microserviceuser.model.User;
import org.agrosfer.pdm.microserviceuser.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


/**
 *
 * @author g3a
 */
@RestController
public class UserController {
    
    @Autowired
    UserDAO userDAO;
    
    @Autowired
    RoleDAO roleDAO;
    
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    ApplicationPropertiesConfig apc;
    
//    @Autowired
//    UserMapper userMapper;
    
    //static byte[] SAL;
    
    @PostMapping("/users")
    public ResponseEntity<?> createUser(@RequestBody User user) throws NoSuchAlgorithmException, NoSuchProviderException{
        
        Optional<User> oneUser = userDAO.findByLogin(user.getLogin().toLowerCase());
        if (oneUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This login is already used");
        }
        
        PassGen newPassGen = encrypt(user.getPassword());
        if (newPassGen.getSecurePassword() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User cannot be created. Go to check microservice-utils");
        }
        
        user.setLogin(user.getLogin().toLowerCase());
        //System.out.println("Login : "+user.getLogin().toLowerCase());
        //System.out.println("Password : "+user.getPassword());
        
        user.setPassword(newPassGen.getSecurePassword());
        user.setSalt(newPassGen.getSalt());
        
        user.setCreateAt(Timestamp.from(Instant.now()));
        user.setUpdateAt(Timestamp.from(Instant.now()));
        user.setIsDelete(Boolean.FALSE);
        user.setStatus(UserStatus.BEING_CREATED);
        user.setIsActivated(Boolean.FALSE);
        user.setIsAdmin(Boolean.FALSE);

        String ref = "";
        ref = getReference(User.class.getSimpleName());

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User cannot be created. Go to check microservice-codification");
        }

        user.setReference(ref);
        User newUser = userDAO.save(user);

        if (newUser != null) {
            UserHelper aUser = UserMapper.fromUser(newUser);
            
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/users/" + aUser.getReference()))
                    .body(aUser);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User cannot be created");
        
    }
    
    @PostMapping("/users/login")
    public ResponseEntity<?> loginUser(@RequestBody User user) throws NoSuchAlgorithmException, NoSuchProviderException{

        Optional<User> oneUser;
        oneUser = userDAO.findByLogin(user.getLogin().toLowerCase());
        
        if (oneUser.isPresent()) {
            
            if (oneUser.get().getIsActivated().equals(Boolean.FALSE)) {
                return ResponseEntity.status(HttpStatus.PRECONDITION_FAILED).body("Account is not activated");
            }
            
            PassGen aPassGen = new PassGen(user.getPassword(), oneUser.get().getSalt());
            String passwordDecrypt = decrypt(aPassGen);
            if (passwordDecrypt == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User cannot be created. Go to check microservice-utils");
            }
            
            if (oneUser.get().getPassword().equals(passwordDecrypt)) {
                UserHelper aUser = UserMapper.fromUser(oneUser.get());
                return ResponseEntity.ok(aUser);
            }
            
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incorrect password");
            
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("This user don't exist");
    }
    
    @PostMapping("/users/register")
    public ResponseEntity<?> registerUser(@RequestBody UserRegisterHelper user) throws NoSuchAlgorithmException, NoSuchProviderException{
        User u = new User();
        if (user.getIsCompany()) {
            u.setLogin(user.getCompanyName().toLowerCase());
            u.setPassword(user.getPassword());
        }
        u.setLogin(user.getPhone());
        u.setPassword(user.getPassword());
        
        ResponseEntity<?> createClient = createClient(user);
        
        if (createClient.getStatusCode() == HttpStatus.ACCEPTED) {
            ClientBean client = ClientBean.class.cast(createClient.getBody());
            System.out.println("client.getReference() : "+ client.getReference());
            u.setClientReference(client.getReference());
            
            ResponseEntity<?> newUser = createUser(u);
            return ResponseEntity.status(newUser.getStatusCode()).body(newUser.getBody());
        }
        return ResponseEntity.status(createClient.getStatusCode()).body(createClient.getBody());
    }
    
    @PostMapping("/users/register/password")
    public ResponseEntity<?> registerPassword(@RequestBody UserRegisterHelper user) throws NoSuchAlgorithmException, NoSuchProviderException{
        User u = new User();
        if (user.getIsCompany()) {
            ResponseEntity<?> getClient = getClientByCompanyName(user);
            if (getClient.getStatusCode() == HttpStatus.OK) {
                String clientRef = ClientBean.class.cast(getClient.getBody()).getReference();
                
                u.setLogin(user.getLogin().toLowerCase());
                u.setPassword(user.getPassword());
                u.setClientReference(clientRef);
                
                
                createUser(u);
                ResponseEntity<?> activatedUser = activateUser(clientRef);
                
                if (activatedUser.getStatusCode() == HttpStatus.OK) {
                    return ResponseEntity.status(activatedUser.getStatusCode()).body("SUCCESS");
                }
                
                return ResponseEntity.status(activatedUser.getStatusCode()).body("FAILURE");
            }
            return ResponseEntity.status(getClient.getStatusCode()).body(getClient.getBody());
        }else{
            ResponseEntity<?> getClient = getClientByPhone(user);
            if (getClient.getStatusCode() == HttpStatus.OK) {
                String clientRef = ClientBean.class.cast(getClient.getBody()).getReference();
                
                u.setLogin(user.getLogin().toLowerCase());
                u.setPassword(user.getPassword());
                u.setClientReference(clientRef);
                
                createUser(u);
                ResponseEntity<?> activatedUser = activateUser(clientRef);
                
                if (activatedUser.getStatusCode() == HttpStatus.OK) {
                    return ResponseEntity.status(activatedUser.getStatusCode()).body("SUCCESS");
                }
                
                return ResponseEntity.status(activatedUser.getStatusCode()).body("FAILURE");
            }
            return ResponseEntity.status(getClient.getStatusCode()).body(getClient.getBody());
            
        }
        
    }
    
    //@PostMapping("/test")
    public ResponseEntity<?> test(@RequestBody ClientBean cb){
//        ReferenceBean rb = new ReferenceBean();
//        rb.setEntity(cb.getLastName());
//
//        //Method1
//        HttpEntity<?> request = new HttpEntity<>(rb);
//        ResponseEntity<ReferenceBean> re;
//        try {
//            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
//            return re.getBody().getCode();
//
//        } catch (RestClientException e) {
//            return ("");
//        }
//        
        
        //Method1
        
        HttpEntity<?> request = new HttpEntity<>(cb);
        ResponseEntity<ClientBean> re;
        ResponseEntity<?> out = null;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsClientCreateUrl()), HttpMethod.POST, request, ClientBean.class);
//            re = restTemplate.exchange(URI.create("http://62.171.183.73:9055/clients"), HttpMethod.POST, request, ClientBean.class);
            return re;

        } catch (RestClientException e) {
            if (e.getMessage().contains("Connection refused")) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cannot create client. Go to check microservice-client");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("\n Error : "+e.getMessage());
            //return out.ok(e.getMessage());
        }
    }
    
    @GetMapping("/users")
    public ResponseEntity<?> findUsers(){
        
        List<User> allUsers = userDAO.findAll();
        List<UserHelper> allUsersHelper = new ArrayList<>();
        for (User user : allUsers) {
            allUsersHelper.add(UserMapper.fromUser(user));
        }
        
        return ResponseEntity.ok(allUsersHelper);
    }
    
    @GetMapping("/users/{ref}")
    public ResponseEntity<?> getUser(@PathVariable("ref") String ref){
        
        Optional<User> oneUser;
        oneUser = userDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneUser.isPresent()) {
            return ResponseEntity.of(oneUser);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This user does not exist");
        
    }
    
    @GetMapping("/users/{ref}/roles")
    public ResponseEntity<?> getRolesUser(@PathVariable("ref") String ref){
        
        Optional<User> oneUser;
        oneUser = userDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneUser.isPresent()) {
            return ResponseEntity.ok(oneUser.get().getRoles());
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This user does not exist");
    }
    
//    @PutMapping("/users")
    @PostMapping("/users/update")
    public ResponseEntity<?> updateUser(@RequestBody User user){
        
        Optional<User> oneUser = userDAO.findByReferenceAndIsDeleteFalse(user.getReference());
        
        if (oneUser.isPresent()) {
            oneUser.get().setPassword(user.getPassword());
            oneUser.get().setIsAdmin(user.getIsAdmin());
            
            oneUser.get().setUpdateAt(Timestamp.from(Instant.now()));
        }
        
        User updatedUser = userDAO.saveAndFlush(oneUser.get());
        
        if (updatedUser != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/users/" + updatedUser.getReference()))
                    .body(updatedUser);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User cannot be updated");
        
    }
    
//    @RequestMapping(value = "/users/{ref}/activate", method = RequestMethod.PUT)
    @PostMapping("/users/{ref}/activate")
    public ResponseEntity<?> activateUser(@PathVariable("ref") String ref){
        
        Optional<User> oneUser = userDAO.findByReferenceAndIsDeleteFalse(ref);
        
        UserStatus us = UserStatus.BEING_CREATED;
        
        if (oneUser.isPresent()) {
            if (us.valid(oneUser.get().getStatus(), UserStatus.ACTIVATED)) {
                oneUser.get().setStatus(UserStatus.ACTIVATED);
                oneUser.get().setIsActivated(Boolean.TRUE);
                User activatedUser = null;
                activatedUser = userDAO.saveAndFlush(oneUser.get());
                if (activatedUser == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("User cannot been activated. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("User is activated successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + oneUser.get().getStatus()+ " to " + UserStatus.ACTIVATED + " is not valid.");
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This user does not exist.");
        
    }
    
//    @PutMapping("/users/{ref}/suspend")
    @PostMapping("/users/{ref}/suspend")
    public ResponseEntity<?> suspendUser(@PathVariable("ref") String ref){
        
        Optional<User> oneUser = userDAO.findByReferenceAndIsDeleteFalse(ref);
        
        UserStatus us = UserStatus.BEING_CREATED;
        
        if (oneUser.isPresent()) {
            if (us.valid(oneUser.get().getStatus(), UserStatus.SUSPEND)) {
                oneUser.get().setStatus(UserStatus.SUSPEND);
                oneUser.get().setIsActivated(Boolean.FALSE);
                User suspendedUser = null;
                suspendedUser = userDAO.saveAndFlush(oneUser.get());
                if (suspendedUser == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("User cannot been suspended. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("User is suspended successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + oneUser.get().getStatus()+ " to " + UserStatus.SUSPEND + " is not valid.");
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This user does not exist.");
        
    }
    
//    @PutMapping("/users/{ref}/deactivate")
    @PostMapping("/users/{ref}/deactivate")
    public ResponseEntity<?> deactivateUser(@PathVariable("ref") String ref){
        
        Optional<User> oneUser = userDAO.findByReferenceAndIsDeleteFalse(ref);
        
        UserStatus us = UserStatus.BEING_CREATED;
        
        if (oneUser.isPresent()) {
            if (us.valid(oneUser.get().getStatus(), UserStatus.DEACTIVATED)) {
                oneUser.get().setStatus(UserStatus.DEACTIVATED);
                oneUser.get().setIsActivated(Boolean.FALSE);
                User deactivatedUser = null;
                deactivatedUser = userDAO.saveAndFlush(oneUser.get());
                if (deactivatedUser == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("User cannot been deactivated. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("User is deactivated successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + oneUser.get().getStatus()+ " to " + UserStatus.DEACTIVATED + " is not valid.");
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This user does not exist.");
        
    }
    
    public String getReference(String entityName) {
        ReferenceBean rb = new ReferenceBean();
        rb.setEntity(entityName);

        //Method1
        HttpEntity<?> request = new HttpEntity<>(rb);
        ResponseEntity<ReferenceBean> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
            return re.getBody().getCode();

        } catch (RestClientException e) {
            return ("");
        }

        //Method2
        //ReferenceBean newR = restTemplate.postForObject(URI.create(apc.getMsCodificationCreateUrl()), rb, ReferenceBean.class);
        //return ResponseEntity.ok(newR);
    }
    
    public PassGen encrypt(String password) {
        
        HttpEntity<?> request = new HttpEntity<>(password);
        ResponseEntity<PassGen> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsUtilsEncryptPasswordUrl()), HttpMethod.POST, request, PassGen.class);
            return re.getBody();

        } catch (RestClientException e) {
            return new PassGen(null, null);
        }
        
    }
    
    public String decrypt(PassGen passGen) {
        
        HttpEntity<?> request = new HttpEntity<>(passGen);
        ResponseEntity<String> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsUtilsDecryptPasswordUrl()), HttpMethod.POST, request, String.class);
            return re.getBody();

        } catch (RestClientException e) {
            return null;
        }
        
    }
    
    public ResponseEntity<?> createClient(UserRegisterHelper user){
        
        ClientBean cb = new ClientBean();
        cb.setFirstName(user.getFirstName());
        cb.setLastName(user.getLastName());
        cb.setPhone(user.getPhone());
        cb.setAddress(user.getAddress());
        cb.setBirthDate(user.getBirthDate());
        cb.setEmail(user.getEmail());
        cb.setWebsite(user.getWebsite());
        cb.setIsCompany(user.getIsCompany());
        cb.setCompanyName(user.getCompanyName().toLowerCase());
        cb.setFieldsOfActivity(user.getFieldsOfActivity());
        cb.setFunctions(user.getFunctions());
        
        HttpEntity<?> request = new HttpEntity<>(cb);
        ResponseEntity<?> response;
        try {
            response = restTemplate.exchange(URI.create(apc.getMsClientCreateUrl()), HttpMethod.POST, request, ClientBean.class);
            return response;

        } catch (RestClientException e) {
            if (e.getMessage().contains("Connection refused")) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cannot create client. Go to check microservice-client");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("\n Error : "+e.getMessage());
        }
        
    }
    
    public ResponseEntity<?> getClientByPhone(UserRegisterHelper user){
        
//        ClientBean cb = new ClientBean();
//        cb.setFirstName(user.getFirstName());
//        cb.setLastName(user.getLastName());
//        cb.setPhone(user.getPhone());
//        cb.setAddress(user.getAddress());
//        cb.setBirthDate(user.getBirthDate());
//        cb.setEmail(user.getEmail());
//        cb.setWebsite(user.getWebsite());
//        cb.setIsCompany(user.getIsCompany());
//        cb.setCompanyName(user.getCompanyName().toLowerCase());
//        cb.setFieldsOfActivity(user.getFieldsOfActivity());
//        cb.setFunctions(user.getFunctions());
        
        ResponseEntity<?> response;
        try {
            response = restTemplate.getForEntity(URI.create(apc.getMsClientCreateUrl()+"/phone/"+user.getLogin()), ClientBean.class);
            return response;

        } catch (RestClientException e) {
            if (e.getMessage().contains("Connection refused")) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cannot get client. Go to check microservice-client");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("\n Error : "+e.getMessage());
        }
        
    }
    
    public ResponseEntity<?> getClientByCompanyName(UserRegisterHelper user){
        
//        ClientBean cb = new ClientBean();
//        cb.setFirstName(user.getFirstName());
//        cb.setLastName(user.getLastName());
//        cb.setPhone(user.getPhone());
//        cb.setAddress(user.getAddress());
//        cb.setBirthDate(user.getBirthDate());
//        cb.setEmail(user.getEmail());
//        cb.setWebsite(user.getWebsite());
//        cb.setIsCompany(user.getIsCompany());
//        cb.setCompanyName(user.getCompanyName().toLowerCase());
//        cb.setFieldsOfActivity(user.getFieldsOfActivity());
//        cb.setFunctions(user.getFunctions());
        
        ResponseEntity<?> response;
        try {
            response = restTemplate.getForEntity(URI.create(apc.getMsClientCreateUrl()+"/company/"+user.getLogin()), ClientBean.class);
            return response;

        } catch (RestClientException e) {
            if (e.getMessage().contains("Connection refused")) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Cannot get client. Go to check microservice-client");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("\n Error : "+e.getMessage());
        }
        
    }
    

    
    //============================== Roles ====================================//
    @PostMapping("/roles")
    public ResponseEntity<?> createRole(@RequestBody Role role){
        
        role.setCreateAt(Timestamp.from(Instant.now()));
        role.setUpdateAt(Timestamp.from(Instant.now()));
        role.setIsDelete(Boolean.FALSE);
        
        Role newRole = roleDAO.save(role);
        
        if (newRole != null) {
            
            return ResponseEntity.accepted()
                    //.location(URI.create(apc.getUrl() + "/roles/" + newRole.getReference()))
                    .body(newRole);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Role cannot be created");
    }
    
    @GetMapping("/roles")
    public ResponseEntity<?> allRoles(){
        return ResponseEntity.ok(roleDAO.findAllByIsDeleteFalse());
    }
    
//    @PutMapping("/roles")
    @PostMapping("/roles/update")
    public ResponseEntity<?> updateRole(@RequestBody Role role){
        
        role.setUpdateAt(Timestamp.from(Instant.now()));
        
        Role updateRole = roleDAO.saveAndFlush(role);
        
        if (updateRole != null) {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(updateRole);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body("Role cannot been update");
    }
    
    @GetMapping("/roles/resources")
    public ResponseEntity<?> allResources(){
        apc.getResources();
        return ResponseEntity.ok(apc.getResources());
    }
    
    @GetMapping("/roles/{designation}/resources")
    public ResponseEntity<?> aRoleResources(@PathVariable("designation") String designation){
        
        Optional<Role> oneRole;
        oneRole = roleDAO.findByDesignationAndIsDeleteFalse(designation);

        if (oneRole.isPresent()) {
            return ResponseEntity.ok(oneRole.get().getResources());
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This role does not exist");
    }
    
}
